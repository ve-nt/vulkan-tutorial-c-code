#pragma once

#include <GLFW/glfw3.h>
#include <vulkan/vulkan.h>

struct State {
    GLFWwindow* window;
    VkInstance vk_instance;
    VkPhysicalDevice physical_device;
};

struct State* stateInit(void);
void stateCleanup(struct State* const state);
