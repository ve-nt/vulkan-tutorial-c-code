#include "state.h"
#include "vulkan/vulkan.h"
#include "window.h"

#include <stdlib.h>

int main(int argc, char* argv[])
{
    (void) argc, (void) argv;
    struct State* state = stateInit();

    /* Hot-loop until we should close the window. */
    while (!glfwWindowShouldClose(state->window)) glfwPollEvents();

    stateCleanup(state);

    return EXIT_SUCCESS;
}
