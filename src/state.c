#include "state.h"

#include "vulkan/vulkan.h"
#include "window.h"

#include <stdlib.h>

/* Initialize program state. Caller owns returned memory. */
struct State* stateInit(void)
{
    struct State* state = malloc(sizeof(struct State));
    state->window = NULL;
    state->vk_instance = NULL;
    state->physical_device = VK_NULL_HANDLE;

    initWindow(state);
    initVulkan(state);

    return state;
}

void stateCleanup(struct State* const state)
{
    deinitVulkan(state);
    deinitWindow(state);
    free(state);
}
