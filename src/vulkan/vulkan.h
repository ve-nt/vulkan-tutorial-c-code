#pragma once

#include "../state.h"

#include <stdbool.h>
#include <vulkan/vulkan.h>

void initVulkan(struct State* const state);
void createInstance(struct State* const state);
void deinitVulkan(struct State* const state);
