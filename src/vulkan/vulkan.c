#include "vulkan.h"

#include "debug.h"
#include "device.h"
#include "extension.h"
#include "layer.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void initVulkan(struct State* const state)
{
    createInstance(state);
    initDebugMessenger(state);
    pickPhysicalDevice(state);
}

void deinitVulkan(struct State* const state)
{
    deinitDebugMessenger(state);

    /* Clean up Vulkan. All other Vulkan resources should be cleaned up before
       destroying the instance. */
    vkDestroyInstance(state->vk_instance, NULL);
}

void createInstance(struct State* const state)
{
    /* Holds some information on our Vulkan instance. This is technically
       optional, but may provide some useful information to the driver so that
       the driver can optimize better.

       This is one of the many struct in Vulkan that requires you to explicitly
       specify the type in the .sType field. It also contains a .pNext field
       that can point to extension information. */
    const VkApplicationInfo app_info = {
        .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pNext = NULL,
        .pApplicationName = "Hello Triangle",
        .applicationVersion = VK_MAKE_VERSION(1, 0, 0),
        .pEngineName = "No Engine",
        .engineVersion = VK_MAKE_VERSION(1, 0, 0),
        .apiVersion = VK_API_VERSION_1_0,
    };

    size_t extensions_n = 0;
    char* const* const extensions = getRequiredExtensions(&extensions_n);

    size_t validation_layers_n = 0;
    const char* const* const validation_layers = getEnabledLayers(&validation_layers_n);

    VkDebugUtilsMessengerCreateInfoEXT debug_messenger_create_info;
    if (ENABLE_VALIDATION_LAYERS) debug_messenger_create_info = populateDebugMessageCreateInfo();

    /* This struct is not optional, and tells the Vulkan driver which global
       extensions and validation layers to use. */
    VkInstanceCreateInfo create_info = {
        .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pApplicationInfo = &app_info,

        /* Global extensions to use. */
        .enabledExtensionCount = extensions_n,
        .ppEnabledExtensionNames = (const char**) extensions,

        /* Global validation layers to use. */
        .enabledLayerCount = validation_layers_n,
        .ppEnabledLayerNames = validation_layers,

        .pNext = ENABLE_VALIDATION_LAYERS ? &debug_messenger_create_info : NULL,
    };

    /* We've specified everything Vulkan needs, and can now create an
       instance. */
    const VkResult result = vkCreateInstance(&create_info, NULL, &state->vk_instance);
    if (result != VK_SUCCESS) fprintf(stderr, "Error: vkCreateInstance\n");

    for (size_t i = 0; i < extensions_n; ++i) free(extensions[i]);
}
