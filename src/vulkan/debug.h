#pragma once

#include "../state.h"

#include <vulkan/vulkan.h>

void initDebugMessenger(struct State* const state);
void deinitDebugMessenger(struct State* const state);
VkDebugUtilsMessengerCreateInfoEXT populateDebugMessageCreateInfo(void);

extern VkDebugUtilsMessengerEXT debug_messenger;
