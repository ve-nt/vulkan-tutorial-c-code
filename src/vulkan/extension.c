#include "extension.h"

#include "layer.h"
#include "vulkan.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static bool extensionsAreSupported(char* const* const extensions, const size_t extensions_n);
static bool extensionIsSupported(
    const char* const extension,
    const VkExtensionProperties* const supported_extensions,
    const size_t supported_extensions_n);
static VkExtensionProperties* getSupportedExtensions(size_t* len);

/* Get the required extension names, ready to be passed to Vulkan. */
char** getRequiredExtensions(size_t* required_extensions_n)
{
    /* GLFW has a handy function that returns the extensions it needs to use. */
    uint32_t glfw_extensions_n = 0;
    const char** glfw_extensions = glfwGetRequiredInstanceExtensions(&glfw_extensions_n);

    *required_extensions_n = glfw_extensions_n;
    char** required_extensions = calloc(sizeof(char*), glfw_extensions_n);
    for (size_t i = 0; i < glfw_extensions_n; ++i) {
        required_extensions[i] = calloc(sizeof(char), strlen(glfw_extensions[i]));
        strcpy(required_extensions[i], glfw_extensions[i]);
    }

    /* We'll add the debug messenger extension for validation layers. */
    if (ENABLE_VALIDATION_LAYERS) {
        ++(*required_extensions_n);
        required_extensions = realloc(required_extensions, sizeof(char*) * *required_extensions_n);
        const size_t last = *required_extensions_n - 1;
        required_extensions[last] = calloc(sizeof(char), strlen(VK_EXT_DEBUG_UTILS_EXTENSION_NAME));
        strcpy(required_extensions[last], VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
    }

    if (!extensionsAreSupported(required_extensions, *required_extensions_n))
        fprintf(stderr, "Error, not all required extensions are supported");

    return required_extensions;
}

bool extensionsAreSupported(char* const* const extensions, const size_t extensions_n)
{
    bool supported = true;

    size_t supported_extensions_n = 0;
    VkExtensionProperties* supported_extensions = getSupportedExtensions(&supported_extensions_n);

    for (size_t i = 0; i < extensions_n; ++i)
        if (!extensionIsSupported(extensions[i], supported_extensions, supported_extensions_n))
            supported = false;

    free(supported_extensions);

    return supported;
}

bool extensionIsSupported(
    const char* const extension,
    const VkExtensionProperties* const supported_extensions,
    const size_t supported_extensions_n)
{
    for (size_t i = 0; i < supported_extensions_n; ++i)
        if (!strcmp(extension, supported_extensions[i].extensionName)) return true;

    return false;
}

/* Get a list of supported Vulkan extensions. `len` will be mutated to the
   number of extensions. Caller owns returned memory. */
VkExtensionProperties* getSupportedExtensions(size_t* len)
{
    /* We can query how many extensions there are by passing the last argument
       as NULL. */
    vkEnumerateInstanceExtensionProperties(NULL, (uint32_t*) len, NULL);

    /* Now we can query and store our extension array. */
    VkExtensionProperties* extensions = calloc(sizeof(*extensions), *len);
    vkEnumerateInstanceExtensionProperties(NULL, (uint32_t*) len, extensions);

    return extensions;
}
