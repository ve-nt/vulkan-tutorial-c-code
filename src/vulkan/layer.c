#include "layer.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static VkLayerProperties* getSupportedLayers(size_t* len);
static bool verifyLayers(
    const char* const* const selected_layers,
    const size_t selected_layers_n,
    const VkLayerProperties* const available_layers,
    const size_t available_layers_n);
static bool verifyLayer(
    const char* const selected_layer_name,
    const VkLayerProperties* const available_layers,
    const size_t available_layers_n);

const char* const validation_layers[] = {
    "VK_LAYER_KHRONOS_validation",
};
size_t validation_layers_n = sizeof(validation_layers) / sizeof(validation_layers[0]);

/* Get the validation layer names, ready to be passed to Vulkan. */
const char* const* getEnabledLayers(size_t* enabled_layers_n)
{
    /* Enable validation layers */
    if (!ENABLE_VALIDATION_LAYERS) {
        *enabled_layers_n = 0;
        return NULL;
    }

    size_t supported_layers_n = 0;
    VkLayerProperties* const supported_layers = getSupportedLayers(&supported_layers_n);
    const bool all_layers_supported = verifyLayers(
        validation_layers, validation_layers_n, supported_layers, supported_layers_n);
    free(supported_layers);

    if (!all_layers_supported) fprintf(stderr, "Not all validation layers supported!\n");

    *enabled_layers_n = validation_layers_n;
    return validation_layers;
}

/* Get a list of all supported Vulkan validation layers. Mutate `len` to the
   numer of layers. Caller owns returned memory. */
VkLayerProperties* getSupportedLayers(size_t* len)
{
    vkEnumerateInstanceLayerProperties((uint32_t*) len, NULL);
    VkLayerProperties* layers = calloc(sizeof(VkLayerProperties), *len);
    vkEnumerateInstanceLayerProperties((uint32_t*) len, layers);
    return layers;
}

/* Verifys all layers in `selected_layers` are in `avaiable_layers`. */
bool verifyLayers(
    const char* const* const selected_layers,
    const size_t selected_layers_n,
    const VkLayerProperties* const available_layers,
    const size_t available_layers_n)
{
    for (size_t i = 0; i < selected_layers_n; ++i) {
        if (!verifyLayer(selected_layers[i], available_layers, available_layers_n)) return false;
    }

    return true;
}

/* Verify that `selected_layer_name` exists in `available_layers`. */
bool verifyLayer(
    const char* const selected_layer_name,
    const VkLayerProperties* const available_layers,
    const size_t available_layers_n)
{
    for (size_t i = 0; i < available_layers_n; ++i) {
        if (!strcmp(selected_layer_name, available_layers[i].layerName)) return true;
    }

    return false;
}
