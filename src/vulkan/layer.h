#pragma once

#include <stdbool.h>
#include <vulkan/vulkan.h>

#ifdef DEBUG
#define ENABLE_VALIDATION_LAYERS true
#else
#define ENABLE_VALIDATION_LAYERS false
#endif

extern const char* const validation_layers[];
extern size_t validation_layers_n;

const char* const* getEnabledLayers(size_t* enabled_layers_n);
