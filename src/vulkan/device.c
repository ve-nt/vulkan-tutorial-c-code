#include "device.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

static bool deviceIsSuitable(const VkPhysicalDevice device);
static void printDevicesInfo(const VkPhysicalDevice* const devices, const size_t devices_n);

void pickPhysicalDevice(struct State* const state)
{
    state->physical_device = VK_NULL_HANDLE;

    size_t devices_n = 0;
    vkEnumeratePhysicalDevices(state->vk_instance, (uint32_t*) &devices_n, NULL);

    if (devices_n == 0) fprintf(stderr, "Error: no GPUs with Vulkan support\n");

    VkPhysicalDevice* devices = calloc(sizeof(VkPhysicalDevice), devices_n);
    vkEnumeratePhysicalDevices(state->vk_instance, (uint32_t*) &devices_n, devices);

    printDevicesInfo(devices, devices_n);

    for (size_t i = 0; i < devices_n; ++i) {
        if (deviceIsSuitable(devices[i])) {
            state->physical_device = devices[i];
            break;
        }
    }

    if (state->physical_device == VK_NULL_HANDLE)
        fprintf(stderr, "Error: No suitable devices found\n");

    free(devices);
}

bool deviceIsSuitable(VkPhysicalDevice device)
{
    (void) device;
    return true;
}

void printDevicesInfo(const VkPhysicalDevice* const devices, const size_t devices_n)
{
    for (uint32_t i = 0; i < devices_n; ++i) {
        VkPhysicalDeviceProperties prop;
        vkGetPhysicalDeviceProperties(devices[i], &prop);
        printf("%s (vid: %04X, pid: %04X)\n", prop.deviceName, prop.vendorID, prop.deviceID);
    }
}
