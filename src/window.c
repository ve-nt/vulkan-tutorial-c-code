#include "window.h"

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

/* Initialize the GLFW window. */
void initWindow(struct State* const state)
{
    glfwInit();

    /* Tell GLFW not to use a client API. This is needed for use with Vulkan, as
       GLFW was originally written for use with OpenGL. */
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

    /* Resizable windows require some extra care that I haven't covered yet. */
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    /* Fourth parameter allows us to optionally specify the monitor to open on
       (if running fullscreen). The Fifth is only relevant for OpenGL. */
    state->window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Vulkan", NULL, NULL);
}

void deinitWindow(struct State* const state)
{
    /* Clean up GLFW. */
    glfwDestroyWindow(state->window);
    glfwTerminate();
}
